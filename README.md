# Utilisation du tableau de compte rendu des Atelier de coréparation

## Télécharger ce repertoire

C'est par [ici](https://framagit.org/atelier-de-co-reparation-de-nantes/tableau-et-compte-rendu-html/-/archive/master/tableau-et-compte-rendu-html-master.zip) il reste plus qu'a dezippez

## Remplissage du tableau

Copiez le tableau `rapport_atelier_coreparation.ods` dans rendu avec en préfixe la date de l'atelier
exemple : `rendu/19-05-29-rapport_atelier_coreparation.ods`

remplissez le tableau lors de l'atelier:
* l'accueil remplissez la premier feuille `tableau entrée`
* à la sortie remplissez la deuxième feuille `tableau sortie`

vigilance:
* ne pas mettre de majuscule au nom des objets
* ne pas mettre de nombre d'objet, créez une ligne par objet
* a la fin de l'atelier mettre `Pas de retour` sur les ligne qui n'ont pas eux de retour
* pour les personnes qui n'ont pas d'objet et qui veulent juste recevoir les mail laisser la colonnes `Objet` vide

pour des problèmes de contrainte sur le tableau vous ne pouvez pas modifier certaines colonnes, si vous voulez changer les restriction le mot de passe est `atelier`   
Attention si vous modifier le tableau le script est susceptible de ne plus marché

## Faire le compte rendu

### exporter les données
Allez à la dernière feuille l'onglet `export`   
Enregistrer le document dans un sous dossier de rendu avec comme nom la date de l'atelier sous le nom `export.csv`
exemple : `rendu/19-05-29/export.csv`
![sauvegarde](doc/img/sauvegarde.png)
choisissez le format pour le fichier csv
![csv](doc/img/format-csv.png)

### géner le fichier html
vous dever avoir php d'installer (sous debian/ubuntu `sudo apt-get install php-cli`)   
ouvrez un terminal puis allez dans le répertoire ou est le fichier export.csv pour éxécuter le script
exemple :
`super+t`
```bash
$cd tableau-et-compte-rendu-html/rendu/19-05-29
$php ../../html_generator.php >index.html
```
dans un exportateur de fichier ouvrer le fichier index.html avec le navigateur web et voila ! vous avez votre compte rendu!
