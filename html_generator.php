<?php
$réparationTente = 0;
$réparationReussi = 0;
$réparationRéparable = 0;
$réparationDiagnostique = 0;
$réparationSansRetour = 0;
$contentCsvMail = "Name,Given Name,Additional Name,Family Name,Yomi Name,Given Name Yomi,Additional Name Yomi,Family Name Yomi,Name Prefix,Name Suffix,Initials,Nickname,Short Name,Maiden Name,Birthday,Gender,Location,Billing Information,Directory Server,Mileage,Occupation,Hobby,Sensitivity,Priority,Subject,Notes,Language,Photo,Group Membership,E-mail 1 - Type,E-mail 1 - Value,E-mail 2 - Type,E-mail 2 - Value,E-mail 3 - Type,E-mail 3 - Value,Phone 1 - Type,Phone 1 - Value,Phone 2 - Type,Phone 2 - Value,Address 1 - Type,Address 1 - Formatted,Address 1 - Street,Address 1 - City,Address 1 - PO Box,Address 1 - Region,Address 1 - Postal Code,Address 1 - Country,Address 1 - Extended Address,Organization 1 - Type,Organization 1 - Name,Organization 1 - Yomi Name,Organization 1 - Title,Organization 1 - Department,Organization 1 - Symbol,Organization 1 - Location,Organization 1 - Job Description,Website 1 - Type,Website 1 - Value".PHP_EOL;

$poles = [ ];
$file = fopen('export.csv', 'r');
while (($line = fgetcsv($file)) !== FALSE) {
    // echo(var_dump($line));
    // var_dump($line);
    if (!in_array( $line[1],["", "0"])) {
        $réparationTente++ ;
        if ($line[9] == "Réparer") {
            $réparationReussi++;
        }
        if ($line[9] == "Réparable") {
            $réparationRéparable++;
        }
        if ($line[9] == "Diagnostique") {
            $réparationDiagnostique++;
        }
        if ($line[9] == "Pas de retour") {
            $réparationSansRetour++;
        }
        if (isset($poles[$line[2]]['nombre'][$line[9]])) {
            $poles[$line[2]]['nombre'][$line[9]]++;
        }else {
            $poles[$line[2]]['nombre'][$line[9]] = 1;
        }
        if (isset($poles[$line[2]]['data']['objet'][$line[1]])){
            $poles[$line[2]]['data']['objet'][$line[1]]++;
        } else {
            $poles[$line[2]]['data']['objet'][$line[1]] = 1;
        }
    }
    if (!in_array( $line[10],["", "0"])) {
        if ($line[11] == "Bricoleureuse" || $line[11] == "Organisteurise" ) {
            $liste ="2-Bricoleurs-euses ::: 4-Pour infos";
        }
        else {
            $liste ="4-Pour infos";
        }
        $contentCsvMail .= $line[0].',,,,,,,,,,,,,,,,,,,,,,,,,,,,'.$liste.',* ,'.$line[10].',,,,,,,,,,,,,,,,,,,,,,,,,,,'.PHP_EOL;
    }
}
fclose($file);
file_put_contents('mail_'.date('d-m-y').'_coorep.csv', $contentCsvMail);
?>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
</head>
<body>
<p>
Nous avons eux <?php echo $réparationTente; ?> objets,<br/>
nous avons réussi a réparer au moins <?php echo $réparationReussi; ?> objets,<br/>
nous avons eux <?php echo $réparationRéparable; ?> objets  qui sont réparable mais où il manquait quelquechose (pièces, temps ...),<br/>
nous avons dignostiqué des pannes non réparable pour <?php echo $réparationDiagnostique; ?> objets,<br/>
nous n'avons pas eux de retour pour <?php echo $réparationSansRetour; ?> objets<br/>
</p>

<?php foreach ($poles as $pole => $poleValue) { ?>

<div class="pole <?php echo $pole; ?>">
    <h3><?php echo $pole; ?></h3>
    <?php if (isset($poleValue['nombre']["Réparer"])) { echo $poleValue['nombre']["Réparer"]; ?> objet réparer <?php } ?><br/>
    <?php if (isset($poleValue['nombre']["Réparable"])) { echo $poleValue['nombre']["Réparable"]; ?> objet réparable <?php } ?>
    <p>
    <?php
    arsort($poleValue['data']['objet']);
    foreach ($poleValue['data']['objet'] as $objets => $value) {?>
        <span>
        <?php echo $value; ?> <?php echo $objets; ?><br/>
    </span>
    <?php } ?>
    </p>
<div>
<?php } ?>
</body>
